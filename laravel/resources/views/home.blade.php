@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
               <!--  @foreach ($devices as $device)
                    <p>id {{ $device->id }}</p>
                    <p>ряд {{ $device->device_token }}</p>
                    <p>счетчик {{ $device->counter }}</p>
                @endforeach -->
                <div class="panel-body">
                    You are logged in!
                    <canvas id="canvas" height="450" width="600"></canvas>
                </div>
            </div>
        </div>
        <form action="saveApiData/" method="POST" style="display: inline-block;">
      <button type="submit" class="btn btn-danger">
        <i class="fa fa-trash"></i> Обнулить счетчики
      </button>
    </form>
    <form action="updateApiData/" method="POST" style="display: inline-block; margin-top: 20px">
      <button type="submit" class="btn btn-primary">
        <i class="fa fa-trash"></i> Обновить счетчики
      </button>
    </form>
    </div>
</div>
<script>
    var barChartData = {
        labels: [
            @foreach ($devices as $device)
                "{{ $device->device_token }}",
            @endforeach
        ],
        datasets: [{
            label: 'Количество Контейнеров',
            backgroundColor: "rgba(151,187,205,0.5)",
            data: [
                @foreach ($devices as $device)
                    {{ $device->counter }},
                @endforeach
            ],
            borderColor: 'white',
            borderWidth: 1
        },]

    };
var myBar = null;
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
            }
        });
    };

    </script>
@endsection
