-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 192.168.1.171:3306
-- Время создания: Апр 20 2017 г., 00:54
-- Версия сервера: 5.7.13
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `carrier_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL,
  `device_token` text NOT NULL,
  `counter` int(11) NOT NULL DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `devices`
--

INSERT INTO `devices` (`id`, `device_token`, `counter`, `updated_at`, `created_at`) VALUES
(5, '192.168.1.1', 2, '2017-04-19 17:29:13', '2017-04-18 21:00:00'),
(6, '192.168.1.2', 11, '2017-04-18 21:00:00', '2017-04-18 21:00:00'),
(7, '192.168.1.3', 3, '2017-04-18 21:00:00', '2017-04-18 21:00:00'),
(8, '192.168.1.4', 6, '2017-04-18 21:00:00', '2017-04-18 21:00:00'),
(9, '192.168.1.5', 3, '2017-04-18 21:00:00', '2017-04-18 21:00:00'),
(10, '192.168.1.6', 8, '2017-04-19 18:05:52', '2017-04-19 18:05:47'),
(11, '192.168.1.9', 9, '2017-04-19 18:40:16', '2017-04-19 18:40:16'),
(12, '192.168.1.10', 5, '2017-04-19 18:40:23', '2017-04-19 18:40:23'),
(13, '192.168.1.12', 16, '2017-04-19 18:42:15', '2017-04-19 18:40:28'),
(14, '192.168.1.13', 16, '2017-04-19 18:42:01', '2017-04-19 18:41:36');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
