<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
   public function index()
   {
    $devices = Device::all();
    return response()->json($devices);
   }

   public function getDevice($id)
   {
    $devices = Device::find($id);
    return response()->json($devices);
   }

   public function saveDevice(Request $request)
   {
    $devices_token = Device::where("device_token","=",$request->device_token)->first();
    if($devices_token!=NULL){
        $devices_token->counter = $request->counter;
        $devices_token->save();
        return response()->json($devices_token);//true
    }
    else{
        $devices = Device::create($request->all());
        return response()->json($devices);//true
    }
   }
   
}
