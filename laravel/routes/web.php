<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
// */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/', function (Request $request) {
    $input = $request->all();
    return $input;
});
Route::get('/device', 'DeviceController@index');
Route::post('/device', 'DeviceController@saveDevice');
Route::get('/device/{id}', 'DeviceController@getDevice');
Route::get('/saveApiData', 'HomeController@saveApiData');
Route::get('/updateApiData', 'HomeController@saveApiData');
