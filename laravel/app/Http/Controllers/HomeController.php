<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Exception;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::all();
        return view('home', compact('devices'));
    }
    public function saveApiData()
    {   
        $devices = Device::all();
        foreach ($devices as $d) {
            try{
                $device_url = $d->device_token.'/drop';
                // $device_url = '192.168.1.134/drop'; test
                $client = new Client();
                $res = $client->request('POST', $device_url, [
                    'form_params' => [
                        'counter' => '0',
                        'secret' => $d->device_token,
                    ]
                ]);
        }
        catch(\Exception $e){
            return "Не все устройства в сети";
        }
        }

        $result= $res->getBody();
        dd($result);
    }
}

